package com.co.anbora.ecaapp.interactors;

import com.co.anbora.ecaapp.classes.ComboItem;
import com.co.anbora.ecaapp.classes.CtsECAAPP;
import com.co.anbora.ecaapp.classes.CtsTrafoECAAPP;
import com.co.anbora.ecaapp.classes.FincaECAAPP;
import com.co.anbora.ecaapp.classes.LocalidadECAAPP;
import com.co.anbora.ecaapp.classes.ProvinciaECAAPP;
import com.co.anbora.ecaapp.classes.RedECAAPP;
import com.co.anbora.ecaapp.classes.TipoPlantilla;
import com.co.anbora.ecaapp.classes.TrafoECAAPP;
import com.co.anbora.ecaapp.injection.Injection;
import com.co.anbora.ecaapp.interfaces.OperacionRequestResponseListener;
import com.co.anbora.ecaapp.interfaces.interactors.OperacionInteractor;
import com.co.anbora.ecaapp.interfaces.presenters.HttpRequestPresenter;
import com.co.anbora.ecaapp.interfaces.views.HttpRequestView;
import com.co.anbora.ecaapp.presenters.HttpRequestPresenterImp;
import com.co.anbora.ecaapp.readfile.ArchivePlaneImp;
import com.co.anbora.ecaapp.readfile.WorkBookImp;
import com.co.anbora.ecaapp.utils.Util;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Cristian Z. Osia
 */
public class OperacionInteractorImp implements OperacionInteractor, HttpRequestView {

    private HttpRequestPresenter requestPresenter;
    private OperacionRequestResponseListener listener;
    private Timer timer;
    private String fileType;
    private long tiempoInicio;
    private ArchivePlaneImp archivePlane;
    private WorkBookImp workBook;
    private ComboItem item;
    private int total;
    private long limit;
    private long index;

    public OperacionInteractorImp() {
        requestPresenter = new HttpRequestPresenterImp(this, Injection.provideHttpRequestInteractor(), Util.URL_SERVER);
    }

    @Override
    public void procesar(TipoPlantilla plantilla, final OperacionRequestResponseListener listener) {
        this.listener = listener;
        if (verificarTipoArchivo(plantilla)) {
            if (verificarArchivo(plantilla.getFile())) {
                this.listener.cleanGui();
                tiempoInicio = 0;
                iniciarTiempo();
                this.listener.run(true);
                iniciar(plantilla);
            }
        }
    }

    private boolean verificarTipoArchivo(TipoPlantilla plantilla) {
        boolean valido = false;
        switch (plantilla.getItem().getId()) {
            case Util.PLANTILLA_SIN_SELECCION:
                this.listener.error(Util.MSG_SIN_SELECCION);
                valido = false;
                break;
            case Util.PLANTILLA_FINCA:
            case Util.PLANTILLA_CTS_TRAFOS:
            case Util.PLANTILLA_CTS:
            case Util.PLANTILLA_LOCAL:
            case Util.PLANTILLA_PROV:
            case Util.PLANTILLA_RED:
            case Util.PLANTILLA_TRAFO:
                valido = true;
                break;
            default:
                this.listener.error(Util.MSG_DESCONOCIDA);
                valido = false;
                break;
        }
        return valido;
    }

    private boolean verificarArchivo(File file) {
        if (file != null && file.exists()) {
            try {
                fileType = Files.probeContentType(file.toPath());
            } catch (IOException ioException) {
                System.out.println("Error: " + ioException.getMessage());
            }
        }
        return (file != null && file.exists());
    }

    private void iniciarTiempo() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (tiempoInicio > 0) {
                    long diferenciaMils = System.currentTimeMillis() - tiempoInicio;
                    long segundos = diferenciaMils / 1000;
                    long horas = segundos / 3600;
                    segundos -= horas * 3600;
                    long minutos = segundos / 60;
                    segundos -= minutos * 60;
                    String time = (horas <= 9 ? "0" : "") + horas
                            + (minutos <= 9 ? ":0" : ":") + minutos
                            + (segundos <= 9 ? ":0" : ":") + segundos;
                    listener.setTime(time);
                } else {
                    tiempoInicio = System.currentTimeMillis();
                }
            }
        };
        timer = new Timer("proceso");
        timer.scheduleAtFixedRate(task, 0, 1000);
    }

    private void detenerTiempo() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
        }
    }

    private void iniciar(TipoPlantilla plantilla) {
        if (fileType != null && !fileType.isEmpty()) {
            switch (fileType) {
                case Util.TIPO_PLANO:
                    readPlane(plantilla);
                    break;
                case Util.TIPO_XLS:
                    readWorkBook(plantilla);
                    break;
            }
        } else {
            listener.error("Error");
        }
    }

    private void readPlane(TipoPlantilla plantilla) {
        try {
            archivePlane = new ArchivePlaneImp(plantilla.getFile());
            item = plantilla.getItem();
            index = 0;
            if (archivePlane != null) {
                total = archivePlane.getCantidad();
                listener.setMax(total);
            }
            limpiarTemp(item);
        } catch (Exception e) {
            listener.error("Error");
        }
    }

    private void readWorkBook(TipoPlantilla plantilla) {
        try {
            workBook = new WorkBookImp(plantilla.getFile());
            limpiarTemp(item);
        } catch (Exception e) {
            listener.error("Error");
        }
    }

    private void limpiarTemp(ComboItem item) {
        requestPresenter.sendRequestDelete(item, Util.TARGET_DELETE);
    }

    @Override
    public void limpiarTempResponse() {
        procesarDatos();
    }

    private synchronized void procesarDatos() {
        if (total > 0) {
            limit = Long.parseLong(Util.REQUEST_OFFSET);
            if (archivePlane != null) {
                Integer countError = 0;
                switch (item.getId()) {
                    case Util.PLANTILLA_FINCA:
                        ArrayList<Set<FincaECAAPP>> fincas = archivePlane.getFincaSet(limit);
                        Set<FincaECAAPP> setfincasO = fincas.get(0);
                        Set<FincaECAAPP> setfincasE = fincas.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<FincaECAAPP> fincasO = new ArrayList<>();
                        fincasO.addAll(setfincasO);
                        sendData(item, fincasO);
                        break;
                    case Util.PLANTILLA_CTS_TRAFOS:
                        ArrayList<Set<CtsTrafoECAAPP>> ctsTrafos = archivePlane.getCtsTrafoSet(limit);
                        Set<CtsTrafoECAAPP> setCtsTrafosO = ctsTrafos.get(0);
                        Set<CtsTrafoECAAPP> setCtsTrafosE = ctsTrafos.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<CtsTrafoECAAPP> ctsTrafosO = new ArrayList<>();
                        ctsTrafosO.addAll(setCtsTrafosO);
                        sendData(item, ctsTrafosO);
                        break;
                    case Util.PLANTILLA_CTS:
                        ArrayList<Set<CtsECAAPP>> cts = archivePlane.getCtsSet(limit);
                        Set<CtsECAAPP> setCtsO = cts.get(0);
                        Set<CtsECAAPP> setCtsE = cts.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<CtsECAAPP> ctsO = new ArrayList<>();
                        ctsO.addAll(setCtsO);
                        sendData(item, ctsO);
                        break;
                    case Util.PLANTILLA_LOCAL:
                        ArrayList<Set<LocalidadECAAPP>> localidades = archivePlane.getLocalidadSet(limit);
                        Set<LocalidadECAAPP> setLocalidadesO = localidades.get(0);
                        Set<LocalidadECAAPP> setLocalidadesE = localidades.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<LocalidadECAAPP> localidadesO = new ArrayList<>();
                        localidadesO.addAll(setLocalidadesO);
                        sendData(item, localidadesO);
                        break;
                    case Util.PLANTILLA_PROV:
                        ArrayList<Set<ProvinciaECAAPP>> provincias = archivePlane.getProvinciaSet(limit);
                        Set<ProvinciaECAAPP> setProvinciasO = provincias.get(0);
                        Set<ProvinciaECAAPP> setProvinciasE = provincias.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<ProvinciaECAAPP> provinciasO = new ArrayList<>();
                        provinciasO.addAll(setProvinciasO);
                        sendData(item, provinciasO);
                        break;
                    case Util.PLANTILLA_RED:
                        ArrayList<Set<RedECAAPP>> redes = archivePlane.getRedSet(limit);
                        Set<RedECAAPP> setRedesO = redes.get(0);
                        Set<RedECAAPP> setRedesE = redes.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<RedECAAPP> redesO = new ArrayList<>();
                        redesO.addAll(setRedesO);
                        sendData(item, redesO);
                        break;
                    case Util.PLANTILLA_TRAFO:
                        ArrayList<Set<TrafoECAAPP>> trafos = archivePlane.getTrafoSet(limit);
                        Set<TrafoECAAPP> setTrafosO = trafos.get(0);
                        Set<TrafoECAAPP> setTrafosE = trafos.get(1);
                        countError = archivePlane.getCountError();
                        if (countError > 0) {
                            listener.setProgressError(countError);
                        }
                        ArrayList<TrafoECAAPP> trafosO = new ArrayList<>();
                        trafosO.addAll(setTrafosO);
                        sendData(item, trafosO);
                        break;
                }
            }
            index += limit;
        }
    }

    private void finalizar(ComboItem item) {
        requestPresenter.sendRequestFinish(item, Util.TARGET_FINISH);
    }

    @Override
    public void finalizarResponse() {
        detenerTiempo();
        listener.run(false);
    }

    private void sendData(ComboItem item, ArrayList data) {
        requestPresenter.sendRequestData(item, data, Util.TARGET_SEND);
    }

    @Override
    public synchronized void sendDataResponse(Integer ok, ArrayList error) {
        listener.setProgressOk(ok);
        if (error != null && !error.isEmpty()) {
            listener.setProgressError(error.size());
        }
        if (index < total) {
            procesarDatos();
        } else {
            finalizar(item);
        }
    }

}
