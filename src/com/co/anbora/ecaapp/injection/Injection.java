package com.co.anbora.ecaapp.injection;

import com.co.anbora.ecaapp.interfaces.interactors.HttpRequestInteractor;
import com.co.anbora.ecaapp.request.HttpRequestImp;
import com.google.gson.Gson;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

/**
 *
 * @author Cristian Z. Osia
 */
public final class Injection {
    
    public static final Client provideClient(){
        return ClientBuilder.newBuilder().build();
    }
    
    public static final Gson provideGson() {
        return new Gson();
    }
    
    public static final WebTarget provideWebTarget(Client client, String url){
        return client.target(url);
    }
    
    public static final HttpRequestInteractor provideHttpRequestInteractor(){
        return new HttpRequestImp(provideGson());
    }
    
}
