package com.co.anbora.ecaapp.readfile;

import com.co.anbora.ecaapp.classes.CtsECAAPP;
import com.co.anbora.ecaapp.classes.CtsTrafoECAAPP;
import com.co.anbora.ecaapp.classes.FincaECAAPP;
import com.co.anbora.ecaapp.classes.LocalidadECAAPP;
import com.co.anbora.ecaapp.classes.ProvinciaECAAPP;
import com.co.anbora.ecaapp.classes.RedECAAPP;
import com.co.anbora.ecaapp.classes.TrafoECAAPP;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Cristian Z. Osia
 */
public class ArchivePlaneImp {

    private File file;
    private BufferedReader br;
    private Integer countError;

    public ArchivePlaneImp() {

    }

    public ArchivePlaneImp(final File file) throws IOException {
        this.file = file;
        br = new BufferedReader(new FileReader(file));
    }

    public Integer getCountError() {
        return countError;
    }

    public int getCantidad() {
        int count = 0;
        try {
            String linea;
            while ((linea = br.readLine()) != null) {
                if (!linea.contains("NIS_RAD")) {
                    count++;
                }
            }
            br.close();
            br = new BufferedReader(new FileReader(file));
        } catch (IOException ex) {
        }
        return count;
    }

    public ArrayList<ArrayList<FincaECAAPP>> getFinca(long limit) {
        long i = 0;
        String line;
        FincaECAAPP finca = new FincaECAAPP();
        ArrayList<ArrayList<FincaECAAPP>> fincas = new ArrayList();
        ArrayList<FincaECAAPP> fincaO = new ArrayList();
        ArrayList fincaE = new ArrayList();
        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                try {
                    finca = new FincaECAAPP();
                    String[] stk = line.split("\\|");
                    String nisRad = stk[0].replace("\"", "");
                    if (!nisRad.equalsIgnoreCase("NIS_RAD")) {
                        String nic = stk[1].replace("\"", "");
                        String nif = stk[2].replace("\"", "");
                        String cliente = stk[3].replace("\"", "");
                        String direccion = stk[4].replace("\"", "");
                        String cgvSum = stk[5].replace("\"", "");
                        String codCalle = stk[6].replace("\"", "");
                        String codLocal = stk[7].replace("\"", "");
                        String nomLocal = stk[8].replace("\"", "");
                        String codMunic = stk[9].replace("\"", "");
                        String nomMunic = stk[10].replace("\"", "");
                        String codDepto = stk[11].replace("\"", "");
                        String nomDepto = stk[12].replace("\"", "");
                        String codProv = stk[13].replace("\"", "");
                        String nomProv = stk[14].replace("\"", "");
                        String longitud = stk[15].replace("\"", "");
                        if (longitud != null) {
                            longitud = longitud.replace(",", ".");
                        }
                        String latitud = stk[16].replace("\"", "");
                        if (latitud != null) {
                            latitud = latitud.replace(",", ".");
                        }
                        finca.setNisRad(nisRad);
                        finca.setNic(nic);
                        finca.setNif(nif);
                        finca.setCliente(cliente);
                        finca.setDireccion(direccion);
                        finca.setCgvSum(cgvSum);
                        finca.setCodCalle(codCalle);
                        finca.setCodLocal(codLocal);
                        finca.setNomLocal(nomLocal);
                        finca.setCodMunic(codMunic);
                        finca.setNomMunic(nomMunic);
                        finca.setCodDepto(codDepto);
                        finca.setNomDepto(nomDepto);
                        finca.setCodProv(codProv);
                        finca.setNomProv(nomProv);
                        finca.setLongitud(longitud);
                        finca.setLatitud(latitud);
                        fincaO.add(finca);
                    } else {
                        i--;
                    }
                } catch (Exception var27) {
                    finca.setError(true);
                    fincaE.add(finca);
                }
            }
            fincas.add(fincaO);
            fincas.add(fincaE);
        } catch (Exception var28) {
        }
        return fincas;
    }

    public ArrayList<Set<FincaECAAPP>> getFincaSet(long limit) {
        long i = 0;
        countError = 0;
        String line;
        ArrayList<Set<FincaECAAPP>> fincas = new ArrayList();
        Set<FincaECAAPP> fincaO = new HashSet<>();
        Set<FincaECAAPP> fincaE = new HashSet<>();
        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                String[] stk = line.split("\\|");
                String nisRad = stk[0].replace("\"", "");
                if (!nisRad.equalsIgnoreCase("NIS_RAD")) {
                    final FincaECAAPP finca = new FincaECAAPP();
                    try {
                        String nic = stk[1].replace("\"", "");
                        String nif = stk[2].replace("\"", "");
                        String cliente = stk[3].replace("\"", "");
                        String direccion = stk[4].replace("\"", "");
                        String cgvSum = stk[5].replace("\"", "");
                        String codCalle = stk[6].replace("\"", "");
                        String codLocal = stk[7].replace("\"", "");
                        String nomLocal = stk[8].replace("\"", "");
                        String codMunic = stk[9].replace("\"", "");
                        String nomMunic = stk[10].replace("\"", "");
                        String codDepto = stk[11].replace("\"", "");
                        String nomDepto = stk[12].replace("\"", "");
                        String codProv = stk[13].replace("\"", "");
                        String nomProv = stk[14].replace("\"", "");
                        String longitud = stk[15].replace("\"", "");
                        if (longitud != null) {
                            longitud = longitud.replace(",", ".");
                        }
                        String latitud = stk[16].replace("\"", "");
                        if (latitud != null) {
                            latitud = latitud.replace(",", ".");
                        }
                        finca.setNisRad(nisRad);
                        finca.setNic(nic);
                        finca.setNif(nif);
                        finca.setCliente(cliente);
                        finca.setDireccion(direccion);
                        finca.setCgvSum(cgvSum);
                        finca.setCodCalle(codCalle);
                        finca.setCodLocal(codLocal);
                        finca.setNomLocal(nomLocal);
                        finca.setCodMunic(codMunic);
                        finca.setNomMunic(nomMunic);
                        finca.setCodDepto(codDepto);
                        finca.setNomDepto(nomDepto);
                        finca.setCodProv(codProv);
                        finca.setNomProv(nomProv);
                        finca.setLongitud(longitud);
                        finca.setLatitud(latitud);
                        if (!fincaO.add(finca)) {
                            finca.setError(true);
                            fincaE.add(finca);
                            countError++;
                            i--;
                        }
                    } catch (Exception var27) {
                        finca.setError(true);
                        fincaE.add(finca);
                        countError++;
                        i--;
                    }
                } else {
                    i--;
                }
            }
            fincas.add(fincaO);
            fincas.add(fincaE);
        } catch (Exception var28) {
        }
        return fincas;
    }

    public ArrayList<Set<CtsTrafoECAAPP>> getCtsTrafoSet(long limit) {
        long i = 0;
        countError = 0;
        String line;
        ArrayList<Set<CtsTrafoECAAPP>> ctsS = new ArrayList();
        Set<CtsTrafoECAAPP> ctsO = new HashSet<>();
        Set<CtsTrafoECAAPP> ctsE = new HashSet<>();

        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                CtsTrafoECAAPP cts = new CtsTrafoECAAPP();
                try {
                    String[] stk = line.split("\\|");
                    String codTrafo = stk[0].replace("\"", "");
                    if (!codTrafo.equalsIgnoreCase("CODIGO_TRAFO")) {
                        String matTrafo = stk[1].replace("\"", "");
                        String codCt = stk[2].replace("\"", "");
                        String matCt = stk[3].replace("\"", "");
                        String potNominal = stk[4].replace("\"", "");
                        String codSalmt = stk[5].replace("\"", "");
                        String matSalmt = stk[6].replace("\"", "");
                        String nombreSalmt = stk[7].replace("\"", "");
                        String nroCentro = stk[8].replace("\"", "");
                        String empresa = stk[9].replace("\"", "");
                        String nroCmd = stk[10].replace("\"", "");
                        String zona = stk[11].replace("\"", "");
                        String nroMesa = stk[12].replace("\"", "");
                        String delegacion = stk[13].replace("\"", "");
                        String propiedad = stk[14].replace("\"", "");
                        String localizacion = stk[15].replace("\"", "");
                        String latitud = stk[16].replace("\"", "");
                        if (latitud != null) {
                            latitud = latitud.replace(",", ".");
                        }
                        String longitud = stk[17].replace("\"", "");
                        if (longitud != null) {
                            longitud = longitud.replace(",", ".");
                        }
                        cts.setCodTrafo(codTrafo);
                        cts.setMatTrafo(matTrafo);
                        cts.setCodCt(codCt);
                        cts.setMatCt(matCt);
                        cts.setPotNominal(potNominal);
                        cts.setCodSalmt(codSalmt);
                        cts.setMatSalmt(matSalmt);
                        cts.setNombreSalmt(nombreSalmt);
                        cts.setNroCentro(nroCentro);
                        cts.setEmpresa(empresa);
                        cts.setNroCmd(nroCmd);
                        cts.setZona(zona);
                        cts.setNroMesa(nroMesa);
                        cts.setDelegacion(delegacion);
                        cts.setPropiedad(propiedad);
                        cts.setLocalizacion(localizacion);
                        cts.setLatitud(latitud);
                        cts.setLongitud(longitud);
                        if (!ctsO.add(cts)) {
                            cts.setError(true);
                            ctsE.add(cts);
                            countError++;
                            i--;
                        }
                    } else {
                        i--;
                    }
                } catch (Exception var28) {
                    cts.setError(true);
                    ctsE.add(cts);
                    countError++;
                    i--;
                }
            }
            ctsS.add(ctsO);
            ctsS.add(ctsE);
        } catch (Exception var29) {
        }
        return ctsS;
    }

    public ArrayList<Set<CtsECAAPP>> getCtsSet(long limit) {
        long i = 0;
        countError = 0;
        String line;
        ArrayList<Set<CtsECAAPP>> ctsS = new ArrayList();
        Set<CtsECAAPP> ctsO = new HashSet<>();
        Set<CtsECAAPP> ctsE = new HashSet<>();
        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                CtsECAAPP cts = new CtsECAAPP();
                try {
                    String[] stk = line.split("\\|");
                    String codCt = stk[0].replace("\"", "");
                    if (!codCt.equalsIgnoreCase("COD_CT")) {
                        String matCt = stk[1].replace("\"", "");
                        String potenciaInstalada = stk[2].replace("\"", "");
                        String codSm = stk[3].replace("\"", "");
                        String matSm = stk[4].replace("\"", "");
                        String nomSm = stk[5].replace("\"", "");
                        String nroCentro = stk[6].replace("\"", "");
                        String empresa = stk[7].replace("\"", "");
                        String nroCmd = stk[8].replace("\"", "");
                        String zonaTr = stk[9].replace("\"", "");
                        String nroMesa = stk[10].replace("\"", "");
                        String nomDelegada = stk[11].replace("\"", "");
                        String propietario = stk[12].replace("\"", "");
                        String localizacion = stk[13].replace("\"", "");
                        String longitud = stk[14].replace("\"", "");
                        String latitud = stk[15].replace("\"", "");
                        cts.setCod_ct(codCt);
                        cts.setMat_ct(matCt);
                        cts.setPotencia_instalada(potenciaInstalada);
                        cts.setCod_sm(codSm);
                        cts.setMat_sm(matSm);
                        cts.setNom_sm(nomSm);
                        cts.setNro_centro(nroCentro);
                        cts.setEmpresa(empresa);
                        cts.setNro_cmd(nroCmd);
                        cts.setZona_tr(zonaTr);
                        cts.setNro_mesa(nroMesa);
                        cts.setNom_delega(nomDelegada);
                        cts.setPropietario(propietario);
                        cts.setLocalizacion(localizacion);
                        cts.setLongitud(longitud);
                        cts.setLatitud(latitud);
                        if (!ctsO.add(cts)) {
                            cts.setError(true);
                            ctsE.add(cts);
                            countError++;
                            i--;
                        }
                    } else {
                        i--;
                    }
                } catch (Exception var26) {
                    cts.setError(true);
                    ctsE.add(cts);
                    countError++;
                    i--;
                }
            }
            ctsS.add(ctsO);
            ctsS.add(ctsE);
        } catch (Exception var27) {
        }
        return ctsS;
    }

    public ArrayList<Set<LocalidadECAAPP>> getLocalidadSet(long limit) {
        long i = 0;
        countError = 0;
        String line;
        ArrayList<Set<LocalidadECAAPP>> localidades = new ArrayList();
        Set<LocalidadECAAPP> localidadO = new HashSet<>();
        Set<LocalidadECAAPP> localidadE = new HashSet<>();
        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                LocalidadECAAPP localidad = new LocalidadECAAPP();
                try {
                    localidad = new LocalidadECAAPP();
                    String[] stk = line.split("\\|");
                    String codLocal = stk[0].replace("\"", "");
                    if (!codLocal.equalsIgnoreCase("COD_LOCAL")) {
                        String nomLocal = stk[1].replace("\"", "");
                        String codMunic = stk[2].replace("\"", "");
                        String codDepto = stk[3].replace("\"", "");
                        String codProv = stk[4].replace("\"", "");
                        localidad.setCodLocal(codLocal);
                        localidad.setNomLocal(nomLocal);
                        localidad.setCodMunic(codMunic);
                        localidad.setCodDepto(codDepto);
                        localidad.setCodProv(codProv);
                        if (!localidadO.add(localidad)) {
                            localidad.setError(true);
                            localidadE.add(localidad);
                            countError++;
                            i--;
                        }
                    } else {
                        i--;
                    }
                } catch (Exception var15) {
                    localidad.setError(true);
                    localidadE.add(localidad);
                    countError++;
                    i--;
                }
            }
            localidades.add(localidadO);
            localidades.add(localidadE);
        } catch (Exception var16) {
        }
        return localidades;
    }

    public ArrayList<Set<ProvinciaECAAPP>> getProvinciaSet(long limit) {
        long i = 0;
        countError = 0;
        String line;
        ArrayList<Set<ProvinciaECAAPP>> provincias = new ArrayList();
        Set<ProvinciaECAAPP> provinciaO = new HashSet<>();
        Set<ProvinciaECAAPP> provinciaE = new HashSet<>();
        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                ProvinciaECAAPP provincia = new ProvinciaECAAPP();
                try {
                    String[] stk = line.split("\\|");
                    String codProv = stk[0].replace("\"", "");
                    if (!codProv.equalsIgnoreCase("COD_PROV")) {
                        String nomProv = stk[1].replace("\"", "");
                        provincia.setCodProv(codProv);
                        provincia.setNomProv(nomProv);
                        if (!provinciaO.add(provincia)) {
                            provincia.setError(true);
                            provinciaO.add(provincia);
                            countError++;
                            i--;
                        }
                    } else {
                        i--;
                    }
                } catch (Exception var12) {
                    provincia.setError(true);
                    provinciaO.add(provincia);
                    countError++;
                    i--;
                }
            }

            provincias.add(provinciaO);
            provincias.add(provinciaE);
        } catch (Exception var13) {
        }

        return provincias;
    }

    public ArrayList<Set<RedECAAPP>> getRedSet(long limit) {
        long i = 0;
        countError = 0;
        String line;
        ArrayList<Set<RedECAAPP>> redes = new ArrayList();
        Set<RedECAAPP> redO = new HashSet<>();
        Set<RedECAAPP> redE = new HashSet<>();
        try {
            while (++i <= limit && (line = this.br.readLine()) != null) {
                RedECAAPP red = new RedECAAPP();
                try {
                    red = new RedECAAPP();
                    String[] stk = line.split("\\|");
                    String zonaSub = stk[0].replace("\"", "");
                    if (!zonaSub.equalsIgnoreCase("ZONA_SUB")) {
                        String corSub = stk[1].replace("\"", "");
                        String sectorSub = stk[2].replace("\"", "");
                        String codSub = stk[3].replace("\"", "");
                        String nomSub = stk[4].replace("\"", "");
                        String codCircuito = stk[5].replace("\"", "");
                        String nomCircuito = stk[6].replace("\"", "");
                        String codCt = stk[7].replace("\"", "");
                        String nomCt = stk[8].replace("\"", "");
                        String placaBlanca = stk[9].replace("\"", "");
                        String codTrafo = stk[10].replace("\"", "");
                        String nomTrafo = stk[11].replace("\"", "");
                        String placaAmarilla = stk[12].replace("\"", "");
                        red.setZonaSub(zonaSub);
                        red.setCorSub(corSub);
                        red.setSectorSub(sectorSub);
                        red.setCodSub(codSub);
                        red.setNomSub(nomSub);
                        red.setCodCircuito(codCircuito);
                        red.setNomCircuito(nomCircuito);
                        red.setCodCt(codCt);
                        red.setNomCt(nomCt);
                        red.setPlacaBlanca(placaBlanca);
                        red.setCodTrafo(codTrafo);
                        red.setNomTrafo(nomTrafo);
                        red.setPlacaAmarilla(placaAmarilla);
                        if (!redO.add(red)) {
                            red.setError(true);
                            redE.add(red);
                            countError++;
                            i--;
                        }
                    } else {
                        i--;
                    }
                } catch (Exception var23) {
                    red.setError(true);
                    redE.add(red);
                    countError++;
                    i--;
                }
            }
            redes.add(redO);
            redes.add(redE);
        } catch (Exception var24) {
        }
        return redes;
    }
    
    
    public ArrayList<Set<TrafoECAAPP>> getTrafoSet(long limit) {
        long i = 0;
        countError = 0;
        String line;        
        ArrayList<Set<TrafoECAAPP>> trafos = new ArrayList();
        Set<TrafoECAAPP> trafoO = new HashSet<>();
        Set<TrafoECAAPP> trafoE = new HashSet<>();
        try {
            while(++i <= limit && (line = this.br.readLine()) != null) {
                TrafoECAAPP trafo = new TrafoECAAPP();
                try {
                    trafo = new TrafoECAAPP();
                    String[] stk = line.split("\\|");
                    String codTrafo = stk[0].replace("\"", "");
                    if(!codTrafo.equalsIgnoreCase("COD_TRAFO")) {
                        String matricula = stk[1].replace("\"", "");
                        String nisRad = stk[2].replace("\"", "");
                        String nif = stk[3].replace("\"", "");
                        String fechaActual = stk[4].replace("\"", "");
                        String horaActual = stk[5].replace("\"", "");
                        String codTar = stk[6].replace("\"", "");
                        String tipServ = stk[7].replace("\"", "");
                        trafo.setCodTrafo(codTrafo);
                        trafo.setMatricula(matricula);
                        trafo.setNisRad(nisRad);
                        trafo.setNif(nif);
                        trafo.setFechaActual(fechaActual);
                        trafo.setHoraActual(horaActual);
                        trafo.setCodTar(codTar);
                        trafo.setTipServ(tipServ);
                        if (!trafoO.add(trafo)) {
                            trafo.setError(true);
                            trafoE.add(trafo);
                            countError++;
                            i--;
                        }
                    } else {
                        i--;
                    }
                } catch (Exception var18) {
                    trafo.setError(true);
                    trafoE.add(trafo);
                    countError++;
                    i--;
                }
            }
            trafos.add(trafoO);
            trafos.add(trafoE);
        } catch (Exception var19) {
        }
        return trafos;
    }

}
