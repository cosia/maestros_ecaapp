package com.co.anbora.ecaapp.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class PolizaGN {

    private boolean error;
    private String categoria;
    private String clienteTipo;
    private String clienteSituacion;
    private String direccion;
    private String estrato;
    private String fechaContratacion;
    private String fechaPuestaServicio;
    private String medidor;
    private String medidorMarca;
    private String medidorTipo;
    private String municipio;
    private String nombre;
    private String poliza;
    private String tarifa;
    private String telefono;
    private String latitud;
    private String longitud;
    private String malla;
    private String tipoContador;

    public PolizaGN() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getClienteTipo() {
        return clienteTipo;
    }

    public void setClienteTipo(String clienteTipo) {
        this.clienteTipo = clienteTipo;
    }

    public String getClienteSituacion() {
        return clienteSituacion;
    }

    public void setClienteSituacion(String clienteSituacion) {
        this.clienteSituacion = clienteSituacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEstrato() {
        return estrato;
    }

    public void setEstrato(String estrato) {
        this.estrato = estrato;
    }

    public String getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(String fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    public String getFechaPuestaServicio() {
        return fechaPuestaServicio;
    }

    public void setFechaPuestaServicio(String fechaPuestaServicio) {
        this.fechaPuestaServicio = fechaPuestaServicio;
    }

    public String getMedidor() {
        return medidor;
    }

    public void setMedidor(String medidor) {
        this.medidor = medidor;
    }

    public String getMedidorMarca() {
        return medidorMarca;
    }

    public void setMedidorMarca(String medidorMarca) {
        this.medidorMarca = medidorMarca;
    }

    public String getMedidorTipo() {
        return medidorTipo;
    }

    public void setMedidorTipo(String medidorTipo) {
        this.medidorTipo = medidorTipo;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPoliza() {
        return poliza;
    }

    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }

    public String getTarifa() {
        return tarifa;
    }

    public void setTarifa(String tarifa) {
        this.tarifa = tarifa;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getMalla() {
        return malla;
    }

    public void setMalla(String malla) {
        this.malla = malla;
    }

    public String getTipoContador() {
        return tipoContador;
    }

    public void setTipoContador(String tipoContador) {
        this.tipoContador = tipoContador;
    }

}
