package com.co.anbora.ecaapp.classes;

import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class XAsyncHttpResponse<T> {

    private int est;
    private int eDB;
    private String msj;
    private ArrayList<T> datos;

    public int getEst() {
        return est;
    }

    public void setEst(int est) {
        this.est = est;
    }

    public int geteDB() {
        return eDB;
    }

    public void seteDB(int eDB) {
        this.eDB = eDB;
    }

    public String getMsj() {
        return msj;
    }

    public void setMsj(String msj) {
        this.msj = msj;
    }

    public ArrayList<T> getData() {
        return datos;
    }

    public void setData(ArrayList<T> data) {
        this.datos = data;
    }

}
