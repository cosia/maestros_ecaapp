package com.co.anbora.ecaapp.classes;

import java.util.Objects;

/**
 *
 * @author Cristian Z. Osia
 */
public class CtsECAAPP {

    private boolean error;
    private String cod_ct;
    private String mat_ct;
    private String potencia_instalada;
    private String cod_sm;
    private String mat_sm;
    private String nom_sm;
    private String nro_centro;
    private String empresa;
    private String nro_cmd;
    private String zona_tr;
    private String nro_mesa;
    private String nom_delega;
    private String propietario;
    private String localizacion;
    private String longitud;
    private String latitud;
    private String theGeom;

    public CtsECAAPP() {
    }    

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCod_ct() {
        return cod_ct;
    }

    public void setCod_ct(String cod_ct) {
        this.cod_ct = cod_ct;
    }

    public String getMat_ct() {
        return mat_ct;
    }

    public void setMat_ct(String mat_ct) {
        this.mat_ct = mat_ct;
    }

    public String getPotencia_instalada() {
        return potencia_instalada;
    }

    public void setPotencia_instalada(String potencia_instalada) {
        this.potencia_instalada = potencia_instalada;
    }

    public String getCod_sm() {
        return cod_sm;
    }

    public void setCod_sm(String cod_sm) {
        this.cod_sm = cod_sm;
    }

    public String getMat_sm() {
        return mat_sm;
    }

    public void setMat_sm(String mat_sm) {
        this.mat_sm = mat_sm;
    }

    public String getNom_sm() {
        return nom_sm;
    }

    public void setNom_sm(String nom_sm) {
        this.nom_sm = nom_sm;
    }

    public String getNro_centro() {
        return nro_centro;
    }

    public void setNro_centro(String nro_centro) {
        this.nro_centro = nro_centro;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNro_cmd() {
        return nro_cmd;
    }

    public void setNro_cmd(String nro_cmd) {
        this.nro_cmd = nro_cmd;
    }

    public String getZona_tr() {
        return zona_tr;
    }

    public void setZona_tr(String zona_tr) {
        this.zona_tr = zona_tr;
    }

    public String getNro_mesa() {
        return nro_mesa;
    }

    public void setNro_mesa(String nro_mesa) {
        this.nro_mesa = nro_mesa;
    }

    public String getNom_delega() {
        return nom_delega;
    }

    public void setNom_delega(String nom_delega) {
        this.nom_delega = nom_delega;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getTheGeom() {
        return theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.cod_ct);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CtsECAAPP other = (CtsECAAPP) obj;
        if (!Objects.equals(this.cod_ct, other.cod_ct)) {
            return false;
        }
        return true;
    }

}
