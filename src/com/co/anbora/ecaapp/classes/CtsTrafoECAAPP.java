package com.co.anbora.ecaapp.classes;

import java.util.Objects;

/**
 *
 * @author Cristian Z. Osia
 */
public class CtsTrafoECAAPP {
    
    private boolean error;
    private String codTrafo;
    private String matTrafo;
    private String codCt;
    private String matCt;
    private String potNominal;
    private String codSalmt;
    private String matSalmt;
    private String nombreSalmt;
    private String nroCentro;
    private String empresa;
    private String nroCmd;
    private String zona;
    private String nroMesa;
    private String delegacion;
    private String propiedad;
    private String localizacion;
    private String latitud;
    private String longitud;
    private String theGeom;

    public CtsTrafoECAAPP() {
    }

    public boolean isError() {
        return this.error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCodTrafo() {
        return this.codTrafo;
    }

    public void setCodTrafo(String codTrafo) {
        this.codTrafo = codTrafo;
    }

    public String getMatTrafo() {
        return this.matTrafo;
    }

    public void setMatTrafo(String matTrafo) {
        this.matTrafo = matTrafo;
    }

    public String getCodCt() {
        return this.codCt;
    }

    public void setCodCt(String codCt) {
        this.codCt = codCt;
    }

    public String getMatCt() {
        return this.matCt;
    }

    public void setMatCt(String matCt) {
        this.matCt = matCt;
    }

    public String getPotNominal() {
        return this.potNominal;
    }

    public void setPotNominal(String potNominal) {
        this.potNominal = potNominal;
    }

    public String getCodSalmt() {
        return this.codSalmt;
    }

    public void setCodSalmt(String codSalmt) {
        this.codSalmt = codSalmt;
    }

    public String getMatSalmt() {
        return this.matSalmt;
    }

    public void setMatSalmt(String matSalmt) {
        this.matSalmt = matSalmt;
    }

    public String getNombreSalmt() {
        return this.nombreSalmt;
    }

    public void setNombreSalmt(String nombreSalmt) {
        this.nombreSalmt = nombreSalmt;
    }

    public String getNroCentro() {
        return this.nroCentro;
    }

    public void setNroCentro(String nroCentro) {
        this.nroCentro = nroCentro;
    }

    public String getEmpresa() {
        return this.empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNroCmd() {
        return this.nroCmd;
    }

    public void setNroCmd(String nroCmd) {
        this.nroCmd = nroCmd;
    }

    public String getZona() {
        return this.zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getNroMesa() {
        return this.nroMesa;
    }

    public void setNroMesa(String nroMesa) {
        this.nroMesa = nroMesa;
    }

    public String getDelegacion() {
        return this.delegacion;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public String getPropiedad() {
        return this.propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getLocalizacion() {
        return this.localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public String getLatitud() {
        return this.latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return this.longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getTheGeom() {
        return this.theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.codTrafo);
        hash = 59 * hash + Objects.hashCode(this.matTrafo);
        hash = 59 * hash + Objects.hashCode(this.codCt);
        hash = 59 * hash + Objects.hashCode(this.matCt);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CtsTrafoECAAPP other = (CtsTrafoECAAPP) obj;
        if (!Objects.equals(this.codTrafo, other.codTrafo)) {
            return false;
        }
        if (!Objects.equals(this.matTrafo, other.matTrafo)) {
            return false;
        }
        if (!Objects.equals(this.codCt, other.codCt)) {
            return false;
        }
        return true;
    }
    
}
