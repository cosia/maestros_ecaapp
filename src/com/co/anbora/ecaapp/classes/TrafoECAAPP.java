package com.co.anbora.ecaapp.classes;

import java.util.Objects;

/**
 *
 * @author Cristian Z. Osia
 */
public class TrafoECAAPP {

    private boolean error;
    private String codTrafo;
    private String matricula;
    private String nisRad;
    private String nif;
    private String fechaActual;
    private String horaActual;
    private String codTar;
    private String tipServ;

    public TrafoECAAPP() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCodTrafo() {
        return codTrafo;
    }

    public void setCodTrafo(String codTrafo) {
        this.codTrafo = codTrafo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNisRad() {
        return nisRad;
    }

    public void setNisRad(String nisRad) {
        this.nisRad = nisRad;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getFechaActual() {
        return fechaActual;
    }

    public void setFechaActual(String fechaActual) {
        this.fechaActual = fechaActual;
    }

    public String getHoraActual() {
        return horaActual;
    }

    public void setHoraActual(String horaActual) {
        this.horaActual = horaActual;
    }

    public String getCodTar() {
        return codTar;
    }

    public void setCodTar(String codTar) {
        this.codTar = codTar;
    }

    public String getTipServ() {
        return tipServ;
    }

    public void setTipServ(String tipServ) {
        this.tipServ = tipServ;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrafoECAAPP other = (TrafoECAAPP) obj;
        if (!Objects.equals(this.nisRad, other.nisRad)) {
            return false;
        }
        return true;
    }

}
