package com.co.anbora.ecaapp.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public final class ComboItem {
    
    private String id;    
    private String descripcion;

    public ComboItem() {
    }

    public ComboItem(String id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return getDescripcion(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
