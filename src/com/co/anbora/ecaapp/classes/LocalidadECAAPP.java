package com.co.anbora.ecaapp.classes;

import java.util.Objects;

/**
 *
 * @author Cristian Z. Osia
 */
public class LocalidadECAAPP {

    private boolean error;
    private String codLocal;
    private String nomLocal;
    private String codMunic;
    private String codDepto;
    private String codProv;

    public LocalidadECAAPP() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getCodLocal() {
        return codLocal;
    }

    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    public String getNomLocal() {
        return nomLocal;
    }

    public void setNomLocal(String nomLocal) {
        this.nomLocal = nomLocal;
    }

    public String getCodMunic() {
        return codMunic;
    }

    public void setCodMunic(String codMunic) {
        this.codMunic = codMunic;
    }

    public String getCodDepto() {
        return codDepto;
    }

    public void setCodDepto(String codDepto) {
        this.codDepto = codDepto;
    }

    public String getCodProv() {
        return codProv;
    }

    public void setCodProv(String codProv) {
        this.codProv = codProv;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.codLocal);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LocalidadECAAPP other = (LocalidadECAAPP) obj;
        if (!Objects.equals(this.codLocal, other.codLocal)) {
            return false;
        }
        return true;
    }

}
