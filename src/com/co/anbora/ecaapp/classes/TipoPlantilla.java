package com.co.anbora.ecaapp.classes;

import java.io.File;

/**
 *
 * @author Cristian Z. Osia
 */
public class TipoPlantilla {
    
    private ComboItem item;
    private File file;
    //private String path;

    public TipoPlantilla() {
    }    
    
    public TipoPlantilla(ComboItem item, File file) {
        this.item = item;
        this.file = file;
    }

    public ComboItem getItem() {
        return item;
    }

    public void setItem(ComboItem item) {
        this.item = item;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    
}
