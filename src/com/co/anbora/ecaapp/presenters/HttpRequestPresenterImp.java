package com.co.anbora.ecaapp.presenters;

import com.co.anbora.ecaapp.classes.ComboItem;
import com.co.anbora.ecaapp.interfaces.OnHttpRequestResponseRestListener;
import com.co.anbora.ecaapp.interfaces.interactors.HttpRequestInteractor;
import com.co.anbora.ecaapp.interfaces.presenters.HttpRequestPresenter;
import com.co.anbora.ecaapp.interfaces.views.HttpRequestView;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class HttpRequestPresenterImp implements HttpRequestPresenter, OnHttpRequestResponseRestListener {

    private HttpRequestView view;
    private HttpRequestInteractor interactor;
    private String urlServer;

    public HttpRequestPresenterImp(HttpRequestView view, HttpRequestInteractor httpInteractor, String urlServer) {
        init(view);
        this.interactor = httpInteractor;
        this.urlServer = urlServer;
    }

    @Override
    public void init(HttpRequestView view) {
        this.view = view;
    }

    @Override
    public void sendRequestDelete(ComboItem item, String url) {
        interactor.sendRequestDelete(item, urlServer, url, this);
    }

    @Override
    public void sendRequestData(ComboItem item, ArrayList data, String url) {
        interactor.sendRequestData(item, data, urlServer, url, this);
    }

    @Override
    public void sendRequestFinish(ComboItem item, String url) {
        interactor.sendRequestFinish(item, urlServer, url, this);
    }

    @Override
    public void writeLog(String error) {
        if (view != null) {

        }
    }

    @Override
    public void limpiarTempResponse() {
        if (view != null) {
            view.limpiarTempResponse();
        }
    }

    @Override
    public void sendDataResponse(Integer ok, ArrayList error) {
        if (view != null) {
            view.sendDataResponse(ok, error);
        }
    }

    @Override
    public void finalizarResponse() {
        if (view != null) {
            view.finalizarResponse();
        }
    }

}
