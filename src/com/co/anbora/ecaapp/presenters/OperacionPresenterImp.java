package com.co.anbora.ecaapp.presenters;

import com.co.anbora.ecaapp.classes.TipoPlantilla;
import com.co.anbora.ecaapp.interactors.OperacionInteractorImp;
import com.co.anbora.ecaapp.interfaces.OperacionRequestResponseListener;
import com.co.anbora.ecaapp.interfaces.interactors.OperacionInteractor;
import com.co.anbora.ecaapp.interfaces.presenters.OperacionPresenter;
import com.co.anbora.ecaapp.interfaces.views.OperacionView;

/**
 *
 * @author Cristian Z. Osia
 */
public class OperacionPresenterImp implements OperacionPresenter, OperacionRequestResponseListener {
    
    private OperacionInteractor interactor;
    private OperacionView operacionView;

    public OperacionPresenterImp(OperacionView operacionView) {
        initOperacion(operacionView);
        this.interactor = new OperacionInteractorImp();
    }    
    
    @Override
    public void initOperacion(OperacionView operacionView) {
        this.operacionView = operacionView;
    }

    @Override
    public void procesar(TipoPlantilla plantilla) {
        if (operacionView != null) {
            interactor.procesar(plantilla, this);
        }
    }

    @Override
    public void error(String error) {
        if (operacionView != null) {
            operacionView.error(error);
        }
    }

    @Override
    public void cleanGui() {
        if (operacionView != null) {
            operacionView.cleanGui();
        }
    }

    @Override
    public void setTime(String time) {
        if (operacionView != null) {
            operacionView.setTime(time);
        }
    }

    @Override
    public void run(Boolean run) {
        if (operacionView != null) {
            operacionView.run(run);
        }
    }

    @Override
    public void setMax(Integer max) {
        if (operacionView != null) {
            operacionView.setMax(max);
        }
    }

    @Override
    public void setProgressOk(Integer ok) {
        if (operacionView != null) {
            operacionView.setProgressOk(ok);
        }
    }

    @Override
    public void setProgressError(Integer error) {
        if (operacionView != null) {
            operacionView.setProgressError(error);
        }
    }
    
}
