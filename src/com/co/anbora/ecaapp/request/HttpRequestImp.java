package com.co.anbora.ecaapp.request;

import com.co.anbora.ecaapp.classes.ComboItem;
import com.co.anbora.ecaapp.classes.ResponseInterface;
import com.co.anbora.ecaapp.classes.XAsyncHttpResponse;
import com.co.anbora.ecaapp.injection.Injection;
import com.co.anbora.ecaapp.interfaces.OnHttpRequestResponseRestListener;
import com.co.anbora.ecaapp.interfaces.interactors.HttpRequestInteractor;
import com.co.anbora.ecaapp.utils.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class HttpRequestImp implements HttpRequestInteractor {

    private Gson gson;
    private OnHttpRequestResponseRestListener listener;
    //private Client client;

    public HttpRequestImp(Gson gson) {
        this.gson = gson;
    }

    @Override
    public void sendRequestDelete(ComboItem item, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("plantilla", gson.toJson(item));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    if (response != null && !response.trim().isEmpty()) {
                        Type type = new TypeToken<XAsyncHttpResponse<Boolean>>() {
                        }.getType();
                        try {
                            XAsyncHttpResponse<Boolean> resp = new Gson().fromJson(response, type);
                            if (response != null) {
                                if (resp.geteDB() != 200) {
                                    listener.writeLog(resp.getMsj());
                                } else {
                                    switch (resp.getEst()) {
                                        case 200:
                                            break;
                                        case 400:
                                        case 500:
                                            listener.writeLog(resp.getMsj());
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            } else {
                                listener.writeLog(Util.MSG_ERROR_SERVIDOR);
                            }
                        } catch (Exception e) {
                            listener.writeLog(Util.MSG_ERROR_CONEXION);
                        }
                    } else {
                        listener.writeLog(Util.MSG_ERROR_CONEXION);
                    }
                    listener.limpiarTempResponse();
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                    listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
                    listener.limpiarTempResponse();
                }

            });
        } catch (Exception e) {
            this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

    @Override
    public void sendRequestFinish(ComboItem item, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("plantilla", gson.toJson(item));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    if (response != null && !response.trim().isEmpty()) {
                        listener.finalizarResponse();
                    } else {
                        listener.writeLog(Util.MSG_ERROR_CONEXION);
                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                    System.out.println(thrwbl.getMessage());
                    listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
                }

            });
        } catch (Exception e) {
            this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

    @Override
    public void sendRequestData(ComboItem item, ArrayList data, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener) {
        try {
            Client client = Injection.provideClient();
            WebTarget target = Injection.provideWebTarget(client, urlServer);
            this.listener = listener;
            Form form = new Form();
            form.param("plantilla", gson.toJson(item));
            form.param("data", gson.toJson(data));
            target.path(urlParams).request().async().post(Entity.form(form), new InvocationCallback<String>() {
                @Override
                public void completed(String response) {
                    client.close();
                    if (response != null && !response.trim().isEmpty()) {
                        ResponseInterface clientResponse = gson.fromJson(response, ResponseInterface.class);
                        String msj;
                        if ((msj = clientResponse.getMsj()) != null) {
                            listener.writeLog("Error");
                        } else {
                            ArrayList<String> ok = clientResponse.getOk();                            
                            ArrayList<String[]> error = clientResponse.getErrors();
                            listener.sendDataResponse(ok != null ? ok.size() : 0, error);
                        }
                    } else {
                        listener.writeLog(Util.MSG_ERROR_CONEXION);
                    }
                }

                @Override
                public void failed(Throwable thrwbl) {
                    client.close();
                    listener.sendDataResponse(0, data);
                    System.out.println(thrwbl.getMessage());
                    listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
                }

            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
            this.listener.writeLog(Util.MSG_ERROR_INICIO_OPE);
        }
    }

}
