package com.co.anbora.ecaapp.interfaces.presenters;

import com.co.anbora.ecaapp.classes.TipoPlantilla;
import com.co.anbora.ecaapp.interfaces.views.OperacionView;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperacionPresenter {
    
    void initOperacion(OperacionView operacionView);
    void procesar(TipoPlantilla plantilla);
    
}
