package com.co.anbora.ecaapp.interfaces.presenters;

import com.co.anbora.ecaapp.classes.ComboItem;
import com.co.anbora.ecaapp.interfaces.views.HttpRequestView;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface HttpRequestPresenter<T> {
    
    void init(HttpRequestView view);
    void sendRequestDelete(ComboItem item, String url);
    void sendRequestData(ComboItem item, ArrayList<T> data, String url);
    void sendRequestFinish(ComboItem item, String url);
    
}
