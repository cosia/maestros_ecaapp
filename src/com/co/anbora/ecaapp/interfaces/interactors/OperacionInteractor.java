package com.co.anbora.ecaapp.interfaces.interactors;

import com.co.anbora.ecaapp.classes.TipoPlantilla;
import com.co.anbora.ecaapp.interfaces.OperacionRequestResponseListener;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperacionInteractor {
    
    void procesar(TipoPlantilla plantilla, OperacionRequestResponseListener listener);
    
}
