package com.co.anbora.ecaapp.interfaces.interactors;

import com.co.anbora.ecaapp.classes.ComboItem;
import com.co.anbora.ecaapp.interfaces.OnHttpRequestResponseRestListener;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface HttpRequestInteractor<T> {
    
    void sendRequestDelete(ComboItem item, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    void sendRequestData(ComboItem item, ArrayList<T> data, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    void sendRequestFinish(ComboItem item, String urlServer, String urlParams, OnHttpRequestResponseRestListener listener);
    
}
