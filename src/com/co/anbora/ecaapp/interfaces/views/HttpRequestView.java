package com.co.anbora.ecaapp.interfaces.views;

import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface HttpRequestView {
    
    void limpiarTempResponse();
    void finalizarResponse();
    void sendDataResponse(Integer ok, ArrayList error);
    
}
