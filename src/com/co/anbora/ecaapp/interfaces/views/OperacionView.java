package com.co.anbora.ecaapp.interfaces.views;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OperacionView {
    
    void error(String error);
    void cleanGui();
    void setTime(String time);
    void run(Boolean run);
    void setMax(Integer max);
    void setProgressOk(Integer ok);
    void setProgressError(Integer error);
    
}
