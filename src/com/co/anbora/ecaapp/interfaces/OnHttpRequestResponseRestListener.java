package com.co.anbora.ecaapp.interfaces;

import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public interface OnHttpRequestResponseRestListener {
    
    void writeLog(String error);
    void limpiarTempResponse();
    void sendDataResponse(Integer ok, ArrayList error);
    void finalizarResponse();
    
}
