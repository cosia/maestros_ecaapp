package com.co.anbora.ecaapp.gui;

import com.co.anbora.ecaapp.classes.ComboItem;
import com.co.anbora.ecaapp.classes.TipoPlantilla;
import com.co.anbora.ecaapp.interfaces.presenters.OperacionPresenter;
import com.co.anbora.ecaapp.interfaces.views.OperacionView;
import com.co.anbora.ecaapp.presenters.OperacionPresenterImp;
import com.co.anbora.ecaapp.utils.Util;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Cristian Z. Osia
 */
public class FormGui extends javax.swing.JFrame implements OperacionView {

    private OperacionPresenter operacionPresenter;
    private TipoPlantilla plantilla;
    private File file;
    private JFileChooser fileChooser;
    private ComboItem itemSelected;
    private Integer max;
    private Integer countOk;
    private Integer countError;
    private static boolean ERRORREADCONFIG = false;

    public FormGui() {
        initComponents();
        init();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabProceso = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbPlantillas = new javax.swing.JComboBox<>();
        txtNombreFile = new javax.swing.JTextField();
        btnProcesar = new javax.swing.JButton();
        btnBuscarFile = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        pgbProgreso = new javax.swing.JProgressBar();
        lblProceso = new javax.swing.JLabel();
        lblTotalTrabajos = new javax.swing.JLabel();
        lblDivisor = new javax.swing.JLabel();
        lblTrabProcesados = new javax.swing.JLabel();
        lblProcent = new javax.swing.JLabel();
        lblPorcentTrabs = new javax.swing.JLabel();
        lblTiempoTrans = new javax.swing.JLabel();
        lblTiempo = new javax.swing.JLabel();
        lblTrabsError = new javax.swing.JLabel();
        lblIconError = new javax.swing.JLabel();
        lblTrabsOk = new javax.swing.JLabel();
        lblIconOk = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtLog = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle(new Util().NAME_APP);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(414, 343));
        setMinimumSize(new java.awt.Dimension(414, 343));
        setResizable(false);
        setSize(new java.awt.Dimension(343, 414));

        tabProceso.setMaximumSize(new java.awt.Dimension(394, 306));
        tabProceso.setMinimumSize(new java.awt.Dimension(394, 306));

        jPanel2.setBorder(BorderFactory.createTitledBorder("Base de datos"));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Plantilla:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Seleccionar:");

        cmbPlantillas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectedItem(evt);
            }
        });

        txtNombreFile.setEditable(false);

        btnProcesar.setMnemonic('P');
        btnProcesar.setText("Procesar");
        btnProcesar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procesar(evt);
            }
        });

        btnBuscarFile.setText("...");
        btnBuscarFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seachFile(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnProcesar)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbPlantillas, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtNombreFile)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBuscarFile, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbPlantillas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBuscarFile)
                    .addComponent(txtNombreFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(13, 13, 13)
                .addComponent(btnProcesar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(BorderFactory.createTitledBorder("Proceso"));

        lblProceso.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        lblTotalTrabajos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTotalTrabajos.setText("0");

        lblDivisor.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblDivisor.setText("/");

        lblTrabProcesados.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTrabProcesados.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTrabProcesados.setText("0");

        lblProcent.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblProcent.setText("%");

        lblPorcentTrabs.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblPorcentTrabs.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPorcentTrabs.setText("0");

        lblTiempoTrans.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTiempoTrans.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTiempoTrans.setText("00:00:00");

        lblTiempo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTiempo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTiempo.setText("Tiempo:");

        lblTrabsError.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTrabsError.setText("0");

        lblIconError.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblIconError.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/co/anbora/ecaapp/recursos/error-icon.png"))); // NOI18N

        lblTrabsOk.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTrabsOk.setText("0");

        lblIconOk.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblIconOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/co/anbora/ecaapp/recursos/ok-icon.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pgbProgreso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblTrabProcesados, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblDivisor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTotalTrabajos, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblProceso, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblIconOk)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTrabsOk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblIconError)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTrabsError, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblPorcentTrabs, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblProcent))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblTiempo, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblTiempoTrans, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblProceso, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIconError)
                    .addComponent(lblTrabsError, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTrabsOk, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIconOk))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pgbProgreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDivisor, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTotalTrabajos, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTrabProcesados, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblProcent, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblPorcentTrabs, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTiempoTrans)
                    .addComponent(lblTiempo))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabProceso.addTab("Proceso", jPanel1);

        txtLog.setColumns(20);
        txtLog.setRows(5);
        jScrollPane3.setViewportView(txtLog);

        tabProceso.addTab("Log", jScrollPane3);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 153, 153));
        jLabel4.setText("C. Osia");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabProceso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(21, 21, 21))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabProceso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void seachFile(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seachFile
        if (file != null && file.exists()) {
            fileChooser.setSelectedFile(file);
        } else {
            File directory = this.fileChooser.getCurrentDirectory();
            this.fileChooser.setCurrentDirectory(directory);
        }
        int opcion = this.fileChooser.showOpenDialog(this);
        if (opcion == 0) {
            this.file = this.fileChooser.getSelectedFile();
            if (this.file != null) {
                this.txtNombreFile.setText(this.file.getName());
            }
        }
    }//GEN-LAST:event_seachFile

    private void procesar(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procesar
        procesar();
    }//GEN-LAST:event_procesar

    private void selectedItem(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectedItem
        itemSelected = (ComboItem) cmbPlantillas.getSelectedItem();
        if (itemSelected != null) {
            switch (itemSelected.getId()) {
                case Util.PLANTILLA_SIN_SELECCION:
                    lblProceso.setText("");
                    break;
                default:
                    lblProceso.setText(itemSelected.getDescripcion());
                    break;
            }
        }
    }//GEN-LAST:event_selectedItem

    public static void main(String args[]) {
        try {
            try {
                diseno();
                if (args != null && args.length > 0) {
                    Properties properties = new Properties();
                    properties.load(new FileReader(args[0]));
                    Util.URL_SERVER = properties.getProperty("URL_SERVER");
                    Util.REQUEST_OFFSET = properties.getProperty("OFFSET");
                    Util.TARGET_SEND = properties.getProperty("TARGET_SEND");
                    Util.TARGET_DELETE = properties.getProperty("TARGET_DELETE");
                    Util.TARGET_FINISH = properties.getProperty("TARGET_FINISH");
                } else {
                    ERRORREADCONFIG = true;
                }
            } catch (Exception e) {
                System.out.println("Error obteniendo parametros, " + e.getMessage());
            }
            java.awt.EventQueue.invokeLater(() -> {
                (new FormGui()).setVisible(true);
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void diseno() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FormGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FormGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FormGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FormGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarFile;
    private javax.swing.JButton btnProcesar;
    private javax.swing.JComboBox<ComboItem> cmbPlantillas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDivisor;
    private javax.swing.JLabel lblIconError;
    private javax.swing.JLabel lblIconOk;
    private javax.swing.JLabel lblPorcentTrabs;
    private javax.swing.JLabel lblProcent;
    private javax.swing.JLabel lblProceso;
    private javax.swing.JLabel lblTiempo;
    private javax.swing.JLabel lblTiempoTrans;
    private javax.swing.JLabel lblTotalTrabajos;
    private javax.swing.JLabel lblTrabProcesados;
    private javax.swing.JLabel lblTrabsError;
    private javax.swing.JLabel lblTrabsOk;
    private javax.swing.JProgressBar pgbProgreso;
    private javax.swing.JTabbedPane tabProceso;
    private javax.swing.JTextArea txtLog;
    private javax.swing.JTextField txtNombreFile;
    // End of variables declaration//GEN-END:variables

    private void init() {
        ArrayList<ComboItem> array = new Util().getTypePlantillas();
        cmbPlantillas.setModel(new DefaultComboBoxModel<ComboItem>(array.toArray(new ComboItem[0])));
        operacionPresenter = new OperacionPresenterImp(this);
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos Planos (*.txt, *.csv, *.xls, *.xlsx)",
                new String[]{"txt", "csv", "xls", "xlsx"});
        fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(0);
        fileChooser.setFileFilter(filtro);
        checkConfig();
    }

    private void checkConfig() {
        if (ERRORREADCONFIG) {
            cmbPlantillas.setEnabled(false);
            error("Debe especificar el archivo de propiedades");
        }
    }

    private void procesar() {
        if (operacionPresenter != null) {
            if (cmbPlantillas != null) {
                plantilla = new TipoPlantilla((ComboItem) cmbPlantillas.getSelectedItem(), file);
                operacionPresenter.procesar(plantilla);
            }
        }
    }

    @Override
    public void error(String error) {
        JOptionPane.showMessageDialog(this, error, "Carga de datos", 2);
    }

    @Override
    public void cleanGui() {
        pgbProgreso.setValue(0);
        lblPorcentTrabs.setText("0");
        lblTiempoTrans.setText("00:00:00");
        lblTotalTrabajos.setText("0");
        lblTrabProcesados.setText("0");
        lblTrabsOk.setText("0");
        lblTrabsError.setText("0");
        countOk = 0;
        countError = 0;
    }

    @Override
    public void setTime(String time) {
        if (lblTiempoTrans != null && time != null) {
            lblTiempoTrans.setText(time);
        }
    }

    @Override
    public void run(Boolean run) {
        cmbPlantillas.setEnabled(!run);
        btnBuscarFile.setEnabled(!run);
        btnProcesar.setEnabled(!run);
    }

    @Override
    public void setMax(Integer max) {
        this.max = max;
        countOk = 0;
        countError = 0;
        pgbProgreso.setValue(0);
        pgbProgreso.setMaximum(max);
        lblTotalTrabajos.setText(max.toString());
    }

    @Override
    public void setProgressOk(Integer ok) {
        countOk += ok;
        Integer trabsProc = (countOk + countError);
        pgbProgreso.setValue(trabsProc);
        lblPorcentTrabs.setText(String.valueOf(((trabsProc) * 100) / max));
        lblTrabsOk.setText(countOk.toString());
        lblTrabProcesados.setText(String.valueOf(trabsProc));
    }

    @Override
    public void setProgressError(Integer error) {
        countError += error;
        Integer trabsProc = (countOk + countError);
        pgbProgreso.setValue(trabsProc);
        lblPorcentTrabs.setText(String.valueOf(((trabsProc) * 100) / max));
        lblTrabsError.setText(countError.toString());
        lblTrabProcesados.setText(String.valueOf(trabsProc));
    }

}
