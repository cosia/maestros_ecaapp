package com.co.anbora.ecaapp.utils;

import com.co.anbora.ecaapp.classes.ComboItem;
import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class Util {
    
    public Util() {
    }
    
    //Nombre de la app
    public static final String NAME_APP = "Carga de datos"; //Seleccionar";
    
    //Popiedades
    //public static String URL_SERVER = "http://mienergiapp.extreme.com.co/mienergiappinterface/ws";
    public static String URL_SERVER = "http://experiment.extreme.com.co/interface_xc_ecaapp/ws";
    //public static String URL_SERVER = "http://localhost:8080/interface_xc_ecaapp/ws";
    public static String REQUEST_OFFSET = "100";
    public static String TARGET_SEND = "ecaapp/maestros";
    public static String TARGET_DELETE = "ecaapp/limpiar";
    public static String TARGET_FINISH = "ecaapp/finalizarOp";
    
    //Tipos de de plantillas
    public static final String PLANTILLA_SIN_SELECCION = "00"; //Seleccionar";
    public static final String PLANTILLA_FINCA = "01"; //Fincas";
    public static final String PLANTILLA_LOCAL = "02"; //"Localidades";
    public static final String PLANTILLA_CTS_TRAFOS = "03"; //"CT's_trafos";
    public static final String PLANTILLA_CTS = "04"; //"CT's";
    public static final String PLANTILLA_PROV = "05"; //"Provincias";
    public static final String PLANTILLA_TRAFO = "06"; //"Trafos-suministros";
    public static final String PLANTILLA_RED = "07"; //"Red MT-BT";
    
    //Mensajes
    public static final String MSG_SIN_SELECCION = "Debe seleccionar una plantilla";
    public static final String MSG_DESCONOCIDA = "No se reconoce este tipo de plantilla";
    public static final String MSG_FILE_VACIO = "Debe seleccionar un archivo valido";
    public static final String MSG_ERROR_INICIO_OPE = "ERROR DE COMUNICACION INICIANDO OPERACION";
    public static final String MSG_ERROR_CONEXION = "ERROR DE CONEXION";
    public static final String MSG_ERROR_SERVIDOR = "No hubo respuesta del servidor";
    
    //Tipos de archivos
    public static final String TIPO_PLANO = "text/plain";
    public static final String TIPO_XLS = "No hubo respuesta del servidor";

    public ArrayList<ComboItem> getTypePlantillas() {
        ArrayList<ComboItem> array = new ArrayList<ComboItem>();
        array.add(new ComboItem(PLANTILLA_SIN_SELECCION, "Seleccionar"));
        array.add(new ComboItem(PLANTILLA_FINCA, "Fincas"));
        array.add(new ComboItem(PLANTILLA_LOCAL, "Localidades"));
        array.add(new ComboItem(PLANTILLA_CTS_TRAFOS, "CT's trafos"));
        array.add(new ComboItem(PLANTILLA_CTS, "CT's"));
        array.add(new ComboItem(PLANTILLA_PROV, "Provincias"));
        array.add(new ComboItem(PLANTILLA_TRAFO, "Trafos suministros"));
        array.add(new ComboItem(PLANTILLA_RED, "Red MT-BT"));
        return array;
    }
    
}
